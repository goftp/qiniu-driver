module gitea.com/goftp/qiniu-driver

go 1.12

require (
	github.com/qiniu/api.v6 v6.0.9+incompatible
	github.com/qiniu/bytes v0.0.0-20140728010635-4887e7b2bde3 // indirect
	github.com/qiniu/rpc v0.0.0-20140728010754-30c22466d920 // indirect
	goftp.io/server v0.0.0-20190812034929-9b3874d17690
)
